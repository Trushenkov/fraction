# Программа для выполнения действий с дробями и работы с текстовыми файлами #

###Реализовано :###
* Операции с дробями: сложение, вычитание, умножение, деление
* Сокращение дробей, в случае если это возможно
* Запись результатов вычислений в текстовый документ output.txt 
* Проверка на существование файла с исходными данными
* Запись не совпадающих с шаблоном строк в отдельный текстовый документ error.txt

###Планируется :###
* Оптимизировать код

### Какие файлы содержатся в этом репозитории? ###
[Fraction.java](https://bitbucket.org/Trushenkov/fraction/src/2cdb3a0c768510f049128ce0299e46bf10aa7801/src/ru/tds/fraction/Fraction.java?at=master&fileviewer=file-view-default) - файл-шаблон для создания объекта класса Fraction и содержащий методы для арифметических действий с дробями

[Main.java](https://bitbucket.org/Trushenkov/fraction/src/2cdb3a0c768510f049128ce0299e46bf10aa7801/src/ru/tds/fraction/Main.java?at=master&fileviewer=file-view-default) - файл для проверки работоспособности класса Fraction, чтения исходных данных из файла [input.txt](https://bitbucket.org/Trushenkov/fraction/src/2cdb3a0c768510f049128ce0299e46bf10aa7801/src/ru/tds/fraction/input.txt?at=master&fileviewer=file-view-default) и записи результата в файл [output.txt](https://bitbucket.org/Trushenkov/fraction/src/2cdb3a0c768510f049128ce0299e46bf10aa7801/src/ru/tds/fraction/output.txt?at=master&fileviewer=file-view-default)

[Input.txt](https://bitbucket.org/Trushenkov/fraction/src/31e41af7774c19ab535a8cc8821d65aad69e6ea9/src/ru/tds/fraction/input.txt?at=master&fileviewer=file-view-default) - файл для чтения, содержащий исходные данные программы.

[Output.txt](https://bitbucket.org/Trushenkov/fraction/src/31e41af7774c19ab535a8cc8821d65aad69e6ea9/src/ru/tds/fraction/output.txt?at=master&fileviewer=file-view-default)
 - файл для записи результатов вычисления.

[Error.txt](https://bitbucket.org/Trushenkov/fraction/src/31e41af7774c19ab535a8cc8821d65aad69e6ea9/src/ru/tds/fraction/error.txt?at=master&fileviewer=file-view-default) - файл, в котором записаны номера строк и строки, которые не прошли валидацию.