package ru.tds.fraction;

import java.util.regex.*;
import java.io.*;

/**
 * Класс для проверки работоспособности класса Fraction.
 *
 * @author Трушенков Дмитрий 15ОИТ18.
 */
public class Main {
    public static void main(String[] args) throws IOException {
        File file = new File("src/ru/tds/fraction/input.txt");
        if (!file.exists()) {
            System.err.println("Файл не сущетсвует или указан неверный путь к нему.");
            return;
        }
        BufferedReader original = new BufferedReader(new FileReader(file));
        BufferedWriter result = new BufferedWriter(new FileWriter("src/ru/tds/fraction/output.txt", false));
        BufferedWriter error = new BufferedWriter(new FileWriter("src/ru/tds/fraction/error.txt", false));
        String string;

        for (int i = 1; (string = original.readLine()) != null; i++) {
            if (!isCheckup(string)) {
                error.write(String.valueOf(i) + ")" + string);
                error.write("\r\n");
                continue;
            }
            String[] strings = splitBySpaces(string);
            Fraction firstFraction = saveToFraction(strings[0]);
            Fraction secondFraction = saveToFraction(strings[2]);
            result.write(String.valueOf(i) + ")" + String.valueOf((mathOperation(firstFraction, secondFraction, strings[1]))));
            result.write("\r\n");
        }
        error.close();
        result.close();
        original.close();
    }

    /**
     * Метод преобразует элементы строчного массива в Int'овые и сохраняет их в обьект типа Fraction.
     *
     * @param string строка
     */
    private static Fraction saveToFraction(String string) {
        String[] strings1 = splitBySlash(string);
        return new Fraction(Integer.parseInt(strings1[0]), Integer.parseInt(strings1[1]));
    }

    /**
     * Метод разделяет строку на части по пробелам.
     *
     * @param string строка
     * @return разделеная строка по пробелам.
     */
    private static String[] splitBySpaces(String string) {
        return string.split(" ");
    }

    /**
     * Метод проверяет строку на корректность ввода.
     *
     * @param string строка
     * @return результат проверки
     */
    private static boolean isCheckup(String string) {
        Pattern pattern = Pattern.compile("^-?[1-9][0-9]*/-?[1-9][0-9]*\\s[*:+-]\\s-?[1-9][0-9]*/-?[1-9][0-9]*$");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    /**
     * Метод, который разделяет строку на числитель и знаменатель.
     *
     * @param string строка
     * @return разделенная строка.
     */
    private static String[] splitBySlash(String string) {
        return string.split("/");
    }

    /**
     * Метод вызывает другие методы в зависимости от действия хранящегося в строке string, для выполнения арифметических действий над 1 и 2 обьектом.
     *
     * @param firstFraction  первая дробь
     * @param secondFraction вторая дробь
     * @param string         знак операции
     * @return результат вычислений
     */
    private static Fraction mathOperation(Fraction firstFraction, Fraction secondFraction, String string) {
        switch (string) {
            case "+":
                return firstFraction.summa(secondFraction);
            case "-":
                return firstFraction.difference(secondFraction);
            case ":":
                return firstFraction.div(secondFraction);
            case "*":
                return firstFraction.multiplication(secondFraction);
        }
        return null;
    }

}