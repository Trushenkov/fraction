package ru.tds.fraction;

/**
 * Класс для инициализирования объекта Fraction.
 *
 * @author Трушенков Дмитрий 15ОИТ18.
 */
public class Fraction {
    private int num;
    private int denum;

    public Fraction(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Fraction(int num) {
        this(num, 1);
    }

    public Fraction() {
        this(1, 1);
    }

    @Override
    public String toString() {
        return num + "/" + denum;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }

    /**
     * Метод для сложения двух дробей.
     *
     * @param fraction вторая дробь
     * @return результат вычисления
     */
    public Fraction summa(Fraction fraction) {
        Fraction result = new Fraction();
        if (this.denum == fraction.denum) {
            result.denum = this.denum;
            result.num = this.num + fraction.num;
        } else {
            result.denum = this.denum * fraction.denum;
            result.num = this.num * fraction.denum + fraction.num * this.denum;
        }
        return result.reduction();
    }

    /**
     * Метод для вычитания двух дробей.
     *
     * @param fraction вторая дробь
     * @return разность двух дробей
     */
    public Fraction difference(Fraction fraction) {
        Fraction result = new Fraction();
        if (this.denum == fraction.denum) {
            result.denum = this.denum;
            result.num = this.num - fraction.num;
        } else {
            result.denum = this.denum * fraction.denum;
            result.num = this.num * fraction.denum - fraction.num * this.denum;
        }
        return result.reduction();
    }

    /**
     * Метод для деления двух дробей.
     *
     * @param fraction вторая дробь
     * @return результат вычисления
     */
    public Fraction div(Fraction fraction) {
        Fraction result = new Fraction(this.num * fraction.denum, this.denum * fraction.num);
        return result.reduction();
    }

    /**
     * Метод для умножения двух дробей.
     *
     * @param fraction вторая дробь
     * @return результат вычисления
     */
    public Fraction multiplication(Fraction fraction) {
        Fraction result = new Fraction(this.num * fraction.num, this.denum * fraction.denum);
        return result.reduction();

    }

    /**
     * Метод сокращяет дробь.
     *
     * @return в зависимости от возможности сокращение если дробь можно скратить то сокращяет и возвращяет, если нет то исходный вариант.
     */
    public Fraction reduction() {

        if (this.num < 0 && this.denum < 0) {
            this.denum = -this.denum;
            this.num = -this.num;
        }


        Fraction fraction1 = new Fraction(this.num, this.denum);
        int a = fraction1.gcd();
        if (a > 1) {

            return new Fraction(this.num / a, this.denum / a);

        }

        return new Fraction(this.num, this.denum);
    }

    public int gcd() {
        int a = this.num;
        int b = this.denum;
        while (b != 0) {
            int tmp = a % b;
            a = b;
            b = tmp;
        }
        return a;
    }
}

